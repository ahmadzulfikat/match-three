﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    int playerCombo;
    int playerScore;
    public Text scoreText;
    public Text comboText;
    public Text timeText;

    float timer = 0.0f;

    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        timer += Time.deltaTime;
        int seconds = (int) timer % 60;
        timeText.text = seconds.ToString() ;
    }


    public void GetScore(int point)
    {
        playerScore += point;
        scoreText.text = playerScore.ToString();
    }

    public void GetCombo(int point)
    {
        playerCombo += point;
        comboText.text = playerCombo.ToString() + "x";
    }
    public void ClearCombo()
    {
        comboText.text = "0";
    }
}
